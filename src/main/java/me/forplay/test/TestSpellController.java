package me.forplay.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.util.Duration;
import me.forplay.application.MainApp;
import me.forplay.model.Result;
import me.forplay.model.Word;
import me.forplay.result.ResultController;
import me.forplay.util.Utils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static me.forplay.util.Utils.formatTime;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-07-29
 */
public class TestSpellController implements Initializable {
    @FXML
    public TextField tfInput;
    @FXML
    public Label lbChinese;
    @FXML
    public Label lbTimer;
    @FXML
    public Button btnConfirm;
    @FXML
    public Button btnFinish;
    @FXML
    public Label lbTimeEnd;
    @FXML
    public Label lbWordEnd;
    @FXML
    public Region region1;
    @FXML
    public Region region2;

    List<Word> wordList;
    public static String testerName;
    public static String orderModeSelectedId;
    public static String testModeSelectedId;
    public static Double duration;
    public static Duration elapsed;
    public static int index;
    private Timeline oneSecondsWonder = new Timeline();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Align the buttons to center
        HBox.setHgrow(region1, Priority.ALWAYS);
        HBox.setHgrow(region2, Priority.ALWAYS);

        Path filePath = Utils.getFilePath("vocabulary.json");
        File file = new File(filePath.toUri());
        String readFileToString = null;
        try {
            readFileToString = FileUtils.readFileToString(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        final Type type = new TypeReference<ArrayList<Word>>() {
        }.getType();
        ArrayList<Word> arrayList = new ArrayList(JSON.parseObject(readFileToString, type));
        wordList = arrayList.stream().filter(it -> !it.getStatus().trim().equals("已屏蔽")).collect(Collectors.toList());
        if ("radioBtnRandom".equals(orderModeSelectedId)) {
            Collections.shuffle(wordList);
        }

        if (duration == 0) {
            lbTimer.setText("不限时");
        } else {
            // CountDownTimer
            KeyFrame keyFrame = new KeyFrame(Duration.seconds(1.0), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    elapsed = Duration.seconds(elapsed.toSeconds() - 1);
                    lbTimer.setText(formatTime(elapsed));
                    if (elapsed.toSeconds() < 1) {
                        oneSecondsWonder.stop();
                        lbTimeEnd.setVisible(true);
                        tfInput.setDisable(true);
                        tfInput.setText("");
                        btnConfirm.setDisable(true);
                    }
                }
            });

            oneSecondsWonder = new Timeline(keyFrame);
            oneSecondsWonder.setCycleCount(Timeline.INDEFINITE);
            oneSecondsWonder.play();
        }

        Word wordFirst = wordList.get(0);
        lbChinese.setText(wordFirst.getChinese());

        tfInput.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER) {
                    doTesting();
                }
            }
        });

        btnConfirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                doTesting();
            }
        });

        btnFinish.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                btnConfirm.setDisable(true);

                List<Word> correct = wordList.stream().filter(it -> "正确".equals(it.getStatus())).collect(Collectors.toList());
                long millis = System.currentTimeMillis();
                String pathName = testerName + "_" + LocalDate.now() + "_" + millis + "_test.json";

                List<Word> inputList = wordList.stream().filter(it -> "正确".equals(it.getStatus()) || "错误".equals(it.getStatus())).collect(Collectors.toList());
                Result result = new Result(testerName, "Spell", LocalDate.now(), duration, inputList.size(), correct.size(), inputList, pathName);

                ResultController.result = result;

                String json = JSON.toJSONStringWithDateFormat(result, "yyyy-MM-dd");
                File file = new File(pathName);

                FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/result/result.fxml"));
                try {
                    FileUtils.writeStringToFile(file, json, "utf-8");
                    Node resultNode = fxmlLoader.load();
                    MainApp.root.setCenter(resultNode);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void doTesting() {
        if (index < wordList.size()) {
            Word word = wordList.get(index);
            String text = tfInput.getText().trim();
            word.setInput(text);
            if (text.equals(word.getEnglish())) {
                word.setStatus("正确");
            } else {
                word.setStatus("错误");
            }
            wordList.set(index, word);

            tfInput.setText("");
            tfInput.requestFocus();
            if (index < wordList.size() - 1) {
                lbChinese.setText(wordList.get(index + 1).getChinese());
            }
            index += 1;
        } else {
            doFinish();
        }
    }

    private void doFinish() {
        tfInput.setDisable(true);
        tfInput.setText("");
        lbWordEnd.setVisible(true);
        btnConfirm.setDisable(true);
    }
}
