package me.forplay.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import me.forplay.application.MainApp;
import me.forplay.model.Result;
import me.forplay.model.Word;
import me.forplay.result.ResultController;
import me.forplay.util.Utils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static me.forplay.util.Utils.formatTime;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-07-29
 */
public class TestTranslateController implements Initializable {
    @FXML
    public Label lbTimer;
    @FXML
    public Label lbTimeEnd;
    @FXML
    public Label lbWordEnd;
    @FXML
    public Button btnTrue;
    @FXML
    public Button btnFalse;
    @FXML
    public Button btnFinish;
    @FXML
    public Region region1;
    @FXML
    public Region region2;
    @FXML
    public Label lbEnglish;
    @FXML
    public Label tipTrue;
    @FXML
    public Label tipFalse;
    @FXML
    public Label lbTip;
    @FXML
    public HBox buttonHbox;
    @FXML
    public GridPane testTranslatePane;

    private List<Word> wordList;
    public static String testerName;
    public static String modeSelectedId;
    public static String orderModeSelectedId;
    public static Double duration;
    public static Duration elapsed;
    public static int index;
    private Timeline oneSecondsWonder = new Timeline();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Align the buttons to center
        HBox.setHgrow(region1, Priority.ALWAYS);
        HBox.setHgrow(region2, Priority.ALWAYS);

        Path filePath = Utils.getFilePath("vocabulary.json");
        File file = new File(filePath.toUri());
        String readFileToString = null;
        try {
            readFileToString = FileUtils.readFileToString(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        final Type type = new TypeReference<ArrayList<Word>>() {
        }.getType();
        ArrayList<Word> arrayList = new ArrayList(JSON.parseObject(readFileToString, type));
        wordList = arrayList.stream().filter(it -> !it.getStatus().trim().equals("已屏蔽")).collect(Collectors.toList());
        if ("radioBtnRandom".equals(orderModeSelectedId)) {
            Collections.shuffle(wordList);
        }

        if (duration == 0) {
            lbTimer.setText("不限时");
        } else {
            // CountDownTimer
            KeyFrame keyFrame = new KeyFrame(Duration.seconds(1.0), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    elapsed = Duration.seconds(elapsed.toSeconds() - 1);
                    lbTimer.setText(formatTime(elapsed));
                    if (elapsed.toSeconds() < 1) {
                        oneSecondsWonder.stop();
                        lbTimeEnd.setVisible(true);
                        btnTrue.setDisable(true);
                        btnFalse.setDisable(true);
                    }
                }
            });

            oneSecondsWonder = new Timeline(keyFrame);
            oneSecondsWonder.setCycleCount(Timeline.INDEFINITE);
            oneSecondsWonder.play();
        }

        Word wordFirst = wordList.get(0);
        lbEnglish.setText(wordFirst.getEnglish());

        btnTrue.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("click btn true...");
                doTesting(true);
            }
        });

        btnFalse.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("click btn btnFalse...");
                doTesting(false);
            }
        });

        btnFinish.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                List<Word> filter = wordList.stream().filter(it -> "正确".equals(it.getStatus())).collect(Collectors.toList());
                List<Word> collect = wordList.stream().filter(it -> "正确".equals(it.getStatus()) || "错误".equals(it.getStatus())).collect(Collectors.toList());
                long millis = System.currentTimeMillis();
                String pathName = testerName + "_" + LocalDate.now() + "_" + millis + "_test.json";
                Result result = new Result(testerName, "Translate", LocalDate.now(), duration, collect.size(), filter.size(), collect, pathName);
                ResultController.result = result;

                String json = JSON.toJSONStringWithDateFormat(result, "yyyy-MM-dd");
                File file = new File(pathName);
                FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/result/result.fxml"));
                try {
                    FileUtils.writeStringToFile(file, json, "utf-8");
                    Node resultPane = fxmlLoader.load();
                    MainApp.root.setCenter(resultPane);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void keyHandler(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            System.out.println("press T");
            doTesting(true);
        }
        if (event.getCode() == KeyCode.F) {
            System.out.println("press F");
            doTesting(false);
        }
    }

    private void doTesting(boolean flag) {
        if (index < wordList.size()) {
            if (flag) {
                Word word = wordList.get(index);
                word.setStatus("正确");
                wordList.set(index, word);
                Utils.showTooltip(tipTrue, "答案正确!", Color.GREEN, 3);
            } else {
                Word word = wordList.get(index);
                word.setStatus("错误");
                wordList.set(index, word);
                Utils.showTooltip(tipFalse, "答案错误!", Color.RED, 3);
            }

            if (index < wordList.size() - 1) {
                lbEnglish.setText(wordList.get(index + 1).getEnglish());
            }
            index += 1;
        } else {
            lbWordEnd.setVisible(true);
            btnTrue.setDisable(true);
            btnFalse.setDisable(true);
        }
    }
}
