package me.forplay.model;

import javafx.beans.property.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-09-13
 */
public class Result {
    private final StringProperty name = new SimpleStringProperty();

    public final StringProperty nameProperty() {
        return this.name;
    }

    public final String getName() {
        return this.nameProperty().get();
    }

    public final void setName(final String name) {
        this.nameProperty().set(name);
    }


    private final StringProperty mode = new SimpleStringProperty();

    public final StringProperty modeProperty() {
        return this.mode;
    }

    public final String getMode() {
        return this.modeProperty().get();
    }

    public final void setMode(final String mode) {
        this.modeProperty().set(mode);
    }


    private final StringProperty fileName = new SimpleStringProperty();

    public final StringProperty fileNameProperty() {
        return this.fileName;
    }

    public final String getFileName() {
        return this.fileNameProperty().get();
    }

    public final void setFileName(final String fileName) {
        this.fileNameProperty().set(fileName);
    }


    private LocalDate date;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    private final DoubleProperty duration = new SimpleDoubleProperty();

    public final DoubleProperty durationProperty() {
        return this.duration;
    }

    public final Double getDuration() {
        return this.durationProperty().get();
    }

    public final void setDuration(final Double duration) {
        this.durationProperty().set(duration);
    }


    private final IntegerProperty inputQuantity = new SimpleIntegerProperty();

    public final IntegerProperty inputQuantityProperty() {
        return this.inputQuantity;
    }

    public final Integer getInputQuantity() {
        return this.inputQuantityProperty().get();
    }

    public final void setInputQuantity(final Integer inputQuantity) {
        this.inputQuantityProperty().set(inputQuantity);
    }


    private final IntegerProperty correctQuantity = new SimpleIntegerProperty();

    public final IntegerProperty correctQuantityProperty() {
        return this.correctQuantity;
    }

    public final Integer getCorrectQuantity() {
        return this.correctQuantityProperty().get();
    }

    public final void setCorrectQuantity(final Integer correctQuantity) {
        this.correctQuantityProperty().set(correctQuantity);
    }


    private List<Word> wordList = new ArrayList<>();

    public List<Word> getWordList() {
        return wordList;
    }

    public void setWordList(List wordList) {
        this.wordList = wordList;
    }

    Result() {
    }

    public Result(String name, String mode, LocalDate date, Double duration, Integer inputQuantity,
                  Integer correctQuantity, List<Word> wordList, String fileName) {
        setName(name);
        setMode(mode);
        setDate(date);
        setDuration(duration);
        setInputQuantity(inputQuantity);
        setCorrectQuantity(correctQuantity);
        setWordList(wordList);
        setFileName(fileName);
    }

    public Result(String name, LocalDate date, String mode, String fileName) {
        setName(name);
        setMode(mode);
        setDate(date);
        setFileName(fileName);
    }
}
