package me.forplay.model;

import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-07-29
 */
public class DataModel {
    private final ObservableList<Word> wordList = FXCollections.observableArrayList(word ->
            new Observable[] {word.statusProperty(), word.englishProperty()});

    private final ObjectProperty<Word> currentWord = new SimpleObjectProperty<>(null);

    public ObjectProperty<Word> currentWordProperty() {
        return currentWord;
    }

    public final Word getCurrentWord() {
        return currentWordProperty().get();
    }

    public final void setCurrentWord(Word word) {
        currentWordProperty().set(word);
    }

    public ObservableList<Word> getWordList() {
        return wordList;
    }

    public void loadData(File file) {
        // mock...
        wordList.setAll(

        );
    }

    public void saveData(File file) { }
}
