package me.forplay.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Word {
    private final IntegerProperty id = new SimpleIntegerProperty();

    public final IntegerProperty idProperty() {
        return this.id;
    }

    public final Integer getId() {
        return this.idProperty().get();
    }

    public final void setId(final Integer id) {
        this.idProperty().set(id);
    }


    private final StringProperty status = new SimpleStringProperty();

    public final StringProperty statusProperty() {
        return this.status;
    }

    public final String getStatus() {
        return this.statusProperty().get();
    }

    public final void setStatus(final String status) {
        this.statusProperty().set(status);
    }


    private final StringProperty english = new SimpleStringProperty();

    public final StringProperty englishProperty() {
        return this.english;
    }

    public final String getEnglish() {
        return this.englishProperty().get();
    }

    public final void setEnglish(final String english) {
        this.englishProperty().set(english);
    }


    private final StringProperty chinese = new SimpleStringProperty();

    public final StringProperty chineseProperty() {
        return this.chinese;
    }

    public final String getChinese() {
        return this.chineseProperty().get();
    }

    public final void setChinese(final String chinese) {
        this.chineseProperty().set(chinese);
    }

    private final StringProperty input = new SimpleStringProperty();

    public final StringProperty inputProperty() {
        return this.input;
    }

    public final String getInput() {
        return this.inputProperty().get();
    }

    public final void setInput(final String input) {
        this.inputProperty().set(input);
    }


    private final StringProperty result = new SimpleStringProperty();

    public final StringProperty resultProperty() {
        return this.result;
    }

    public final String getResult() {
        return this.resultProperty().get();
    }

    public final void setResult(final String result) {
        this.resultProperty().set(result);
    }


    public Word(Integer id, String status, String english, String chinese) {
        setId(id);
        setStatus(status);
        setEnglish(english);
        setChinese(chinese);
    }

    public Word(Integer id, String status, String english, String chinese, String result) {
        setId(id);
        setStatus(status);
        setEnglish(english);
        setChinese(chinese);
        setResult(result);
    }
}
