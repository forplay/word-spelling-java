package me.forplay.result;

import com.alibaba.fastjson.JSONObject;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.util.Pair;
import me.forplay.model.Result;
import me.forplay.model.Word;
import me.forplay.util.Utils;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static me.forplay.util.Utils.showTooltip;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-07-30
 */
public class ResultController implements Initializable {
    @FXML
    public Label lbName;
    @FXML
    public Label lbDate;
    @FXML
    public Label lbTestMode;
    @FXML
    public Label lbUsedTime;
    @FXML
    public Label lbInputCount;
    @FXML
    public Label lbCorrectCount;
    @FXML
    public Label lbCorrectRatio;
    @FXML
    public TableView<Word> tableView;
    @FXML
    public TableColumn<Word, Integer> tcId;
    @FXML
    public TableColumn<Word, String> tcChinese;
    @FXML
    public TableColumn<Word, String> tcEnglish;
    @FXML
    public TableColumn<Word, String> tcInput;
    @FXML
    public TableColumn<Word, String> tcStatus;

    //以下为测试结果列表
    @FXML
    public TableView<Result> resultListTableView;
    @FXML
    public TableColumn<Result, String> tcTester;
    @FXML
    public TableColumn<Result, LocalDate> tcDate;
    @FXML
    public TableColumn<Result, String> tcTestMode;
    @FXML
    public TableColumn<Result, String> tcFileName;
    @FXML
    public TableColumn<Result, Result> tcOperation;

    public static Result result;
    public List<Result> resultFileList;
    @FXML
    public Label tipSuccess;
    @FXML
    public Label tipFailed;

    private org.apache.logging.log4j.Logger logger = LogManager.getLogger(this.getClass().getName());

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (result != null) {
            renderResult(result);
        }

        //Render resting result file list
        Collection<File> files = FileUtils.listFiles(new File("."), new String[]{"json"}, true);
        List<File> resultFiles = files.stream().filter(it -> it.getName().endsWith("_test.json"))
                .collect(Collectors.toList());

        resultFiles.forEach(it -> logger.info("result file:" + it.getName()));

        List<String> collect = resultFiles.stream().map(it -> {
            try {
                return FileUtils.readFileToString(it, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }).collect(Collectors.toList());

        List<Result> collect1 = collect.stream().map(it -> JSONObject.parseObject(it, Result.class)).collect(Collectors.toList());
        resultFileList = collect1.stream().map(it -> new Result(it.getName(), it.getDate(), it.getMode(), it.getFileName())).collect(Collectors.toList());


        tcTester.setCellValueFactory(new PropertyValueFactory<>("name"));
        tcDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        tcTestMode.setCellValueFactory(new PropertyValueFactory<>("mode"));
        tcFileName.setCellValueFactory(new PropertyValueFactory<>("fileName"));

        resultListTableView.setItems(FXCollections.observableArrayList(resultFileList));


        //Click on the line to display the content in the table
        resultListTableView.setRowFactory(tv -> {
            TableRow<Result> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY) {
                    Result clickedRow = row.getItem();
                    System.out.println("click row....");
                    String fileName = clickedRow.getFileName();
                    Result result = collect1.stream().filter(it -> it.getFileName().equalsIgnoreCase(fileName)).collect(Collectors.toList()).get(0);
                    List wordList = result.getWordList();

                    Set<Word> set = new HashSet<>();
                    for (Object o11 : wordList) {
                        Word word1 = JSONObject.parseObject(o11.toString(), Word.class);
                        set.add(word1);
                    }

                    List<Word> mainList = new ArrayList<>();
                    mainList.addAll(set);
                    List<Word> wordList1 = mainList.stream().sorted(Comparator.comparing(Word::getId)).collect(Collectors.toList());

                    Result result1 = new Result(result.getName(), result.getMode(), result.getDate(), result.getDuration(),
                            result.getInputQuantity(), result.getCorrectQuantity(), wordList1, result.getFileName());

                    renderResult(result1);
                }
            });
            return row;
        });


    }

    //Render testing result
    private void renderResult(Result result) {
        tcId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcChinese.setCellValueFactory(new PropertyValueFactory<>("chinese"));
        tcEnglish.setCellValueFactory(new PropertyValueFactory<>("english"));
        tcInput.setCellValueFactory(new PropertyValueFactory<>("input"));
        tcStatus.setCellValueFactory(new PropertyValueFactory<>("status"));

        tcStatus.setCellFactory(new Callback<TableColumn<Word, String>, TableCell<Word, String>>() {
            @Override
            public TableCell<Word, String> call(TableColumn<Word, String> column) {
                return new TableCell<Word, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        setText(empty ? "" : getItem().toString());
                        setGraphic(null);
                        TableRow<Word> currentRow = getTableRow();
                        if (!isEmpty()) {
                            tcStatus.setStyle("-fx-alignment: CENTER;");
                            if ("错误".equals(item)) {
                                currentRow.setStyle("-fx-background-color:lightcoral;");
                            }
                            if ("正确".equals(item)) {
                                currentRow.setStyle("-fx-background-color:lightgreen;");
                            }
                        }
                    }
                };
            }
        });


        tcOperation.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Result, Result>, ObservableValue<Result>>() {
            @Override
            public ObservableValue<Result> call(TableColumn.CellDataFeatures<Result, Result> param) {
                return new ReadOnlyObjectWrapper<>(param.getValue());
            }
        });

        tcOperation.setCellFactory(new Callback<TableColumn<Result, Result>, TableCell<Result, Result>>() {
            @Override
            public TableCell<Result, Result> call(TableColumn<Result, Result> param) {
                return new TableCell<Result, Result>() {
                    private final Button deleteButton = new Button("删除");
                    BorderPane borderPane = new BorderPane(deleteButton);

                    @Override
                    protected void updateItem(Result result1, boolean empty) {
                        super.updateItem(result1, empty);

                        if (result1 == null) {
                            setGraphic(null);
                            return;
                        }
                        setGraphic(borderPane);

                        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                String fileName = result1.getFileName();

                                Dialog dialog = new Dialog<Pair<String, String>>();
                                dialog.setHeaderText("是否删除?");

                                Label content = new Label(fileName);
                                Image icon = new Image(getClass().getResourceAsStream("/img/ic_delete_forever_black_48dp.png"));
                                Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
                                stage.getIcons().add(icon);

                                ButtonType confirmButtonType = new ButtonType("删除", ButtonBar.ButtonData.OK_DONE);
                                dialog.getDialogPane().getButtonTypes().addAll(confirmButtonType, ButtonType.CANCEL);

                                dialog.getDialogPane().setContent(content);

                                dialog.setResultConverter(new Callback<ButtonType, Boolean>() {
                                    @Override
                                    public Boolean call(ButtonType dialogButton) {
                                        if (dialogButton == confirmButtonType) {
                                            String fileName1 = result1.getFileName();
                                            File file = new File(fileName1);
                                            if (file.delete()) {
                                                resultFileList.remove(result1);
                                                resultListTableView.getItems().setAll(resultFileList);

                                                lbName.setText("-");
                                                lbDate.setText("-");
                                                lbTestMode.setText("-");
                                                lbUsedTime.setText("-");
                                                lbInputCount.setText("-");
                                                lbCorrectCount.setText("-");
                                                lbCorrectRatio.setText("-");
                                                tableView.getItems().clear();

                                                logger.info("File deleted successfully:" + fileName1);
                                                showTooltip(tipSuccess, "删除成功！", Color.GREEN, 3);
                                                return true;

                                            } else {
                                                logger.error("Failed to delete the file:" + fileName1);
                                                showTooltip(tipSuccess, "删除失败！", Color.RED, 3);
                                                return false;
                                            }
                                        } else
                                            return null;
                                    }
                                });
                                Optional<Boolean> result = dialog.showAndWait();

                                result.ifPresent(new Consumer<Boolean>() {
                                    @Override
                                    public void accept(Boolean b) {
                                    }
                                });

                            }
                        });
                    }
                };
            }
        });


        List<Word> wordList = result.getWordList();
        String name = result.getName();
        LocalDate date = result.getDate();
        String mode = result.getMode();
        Integer correctQuantity = result.getCorrectQuantity();
        Double usedTime = result.getDuration();

        tableView.setItems(FXCollections.observableArrayList(wordList));
        lbName.setText(name);
        lbDate.setText(date.toString());
        lbTestMode.setText(mode);

        if (usedTime == 0.0) {
            lbUsedTime.setText("不限时");
        } else {
            lbUsedTime.setText(Utils.formatTime(new Duration(usedTime * 1000)));
        }

        lbInputCount.setText(Integer.toString(wordList.size()));
        lbCorrectCount.setText(Integer.toString(correctQuantity));
        Double ratio = Double.parseDouble(correctQuantity + "") / wordList.size();
        String format = String.format("%.2f", ratio * 100.0);
        lbCorrectRatio.setText(format + "%");
    }
}
