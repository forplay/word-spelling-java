package me.forplay.start;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Duration;
import me.forplay.application.MainApp;
import me.forplay.test.TestSpellController;
import me.forplay.test.TestTranslateController;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-07-29
 */
public class StartController implements Initializable {
    @FXML
    public GridPane startPane;
    @FXML
    public TextField tfName;
    @FXML
    public ComboBox cbTime;
    @FXML
    public Button btnStart;
    @FXML
    public Label lbName;
    @FXML
    public Label lbTime;
    @FXML
    public RadioButton radioBtnRandom;
    @FXML
    public RadioButton radioBtnOrder;
    @FXML
    public RadioButton radioBtnSpell;
    @FXML
    public RadioButton radioBtnTranslate;
    @FXML
    public ToggleGroup selectMode;
    @FXML
    public ToggleGroup testMode;

    public static String name;
    public Double time;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cbTime.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue observable, Number oldValue, Number newValue) {
                switch (newValue.intValue()) {
                    case 0:
                        time = 30.0;
                        break;
                    case 1:
                        time = 60.0;
                        break;
                    case 2:
                        time = 120.0;
                        break;
                    case 3:
                        time = 180.0;
                        break;
                    case 4:
                        time = 240.0;
                        break;
                    case 5:
                        time = 300.0;
                        break;
                    case 6:
                        time = 0.0;
                        break;
                    default:
                        time = 0.0;
                }
            }
        });

        selectMode = new ToggleGroup();
        selectMode.getToggles().addAll(radioBtnRandom, radioBtnOrder);

        testMode = new ToggleGroup();
        testMode.getToggles().addAll(radioBtnSpell, radioBtnTranslate);
    }

    @FXML
    public void start(ActionEvent event) throws IOException {
        int selectedIndex = cbTime.getSelectionModel().getSelectedIndex();
        if (selectedIndex == -1) {
            lbTime.setVisible(true);
        } else {
            lbTime.setVisible(false);
        }
        name = tfName.getText().trim();

        RadioButton selectedRadioButton = (RadioButton) selectMode.getSelectedToggle();
        String randomId = selectedRadioButton.getId();

        RadioButton testModeSelectedRadioButton = (RadioButton) testMode.getSelectedToggle();
        String idTestMode = testModeSelectedRadioButton.getId();

        TestSpellController.orderModeSelectedId = randomId;
        TestSpellController.testerName = name;
        TestSpellController.duration = time;
        TestSpellController.elapsed = Duration.seconds(time);

        TestTranslateController.orderModeSelectedId = randomId;
        TestTranslateController.testerName = name;
        TestTranslateController.duration = time;
        TestTranslateController.elapsed = Duration.seconds(time);


        boolean isNameEmpty = tfName.getText() == null || tfName.getText().trim().isEmpty();
        lbName.setVisible(isNameEmpty);

        FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/test/testSpell.fxml"));
        if ("radioBtnTranslate".equalsIgnoreCase(idTestMode)) {
            fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/test/testTranslate.fxml"));
        }

        Node testNode = fxmlLoader.load();
        MainApp.root.setCenter(testNode);
    }
}
