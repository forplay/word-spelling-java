package me.forplay.util;

import javafx.animation.FadeTransition;
import javafx.animation.SequentialTransition;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-07-29
 */
public class Utils {
    /**
     * 获取工程根目录下指定的文件的Path路径
     */
    public static Path getFilePath(String fileName) {
        File dir = new File("");
        String projectPath = null;  //获取工程根目录
        try {
            projectPath = dir.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.startsWith("linux")) {
            return Paths.get(projectPath + "/" + fileName);
        } else {//windows
            return Paths.get(projectPath + "\\" + fileName);
        }
    }

    // format time to 23:59:59
    public static String formatTime(Duration elapsed) {
        int intElapsed = (int) Math.floor(elapsed.toSeconds());
        int temp = intElapsed;

        int elapsedHours = intElapsed / (60 * 60);
        if (elapsedHours > 0) {
            intElapsed -= elapsedHours * 60 * 60;
        }
        int elapsedMinutes = intElapsed / 60;
        int elapsedSeconds = (temp - elapsedHours * 60 * 60 - elapsedMinutes * 60);

        return elapsedHours > 0 ? String.format("%d:%02d:%02d", elapsedHours, elapsedMinutes, elapsedSeconds)
                : String.format("%02d:%02d", elapsedMinutes, elapsedSeconds);
    }

    /**
     * @param node      显示的Label
     * @param content   提示内容
     * @param textColor 提示内容颜色
     * @param duration  提示时长，单位：秒
     */
    public static void showTooltip(Label node, String content, Color textColor, double duration) {
        node.setText(content);
        node.setTextFill(textColor);
        node.setVisible(true);
        FadeTransition fadeTransition = new FadeTransition(Duration.seconds(duration), node);
        fadeTransition.setFromValue(1.0);
        fadeTransition.setToValue(0.0);
        SequentialTransition blinkThenFade = new SequentialTransition(node, fadeTransition);
        blinkThenFade.play();
    }
}
