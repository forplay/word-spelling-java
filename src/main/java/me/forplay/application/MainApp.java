package me.forplay.application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import me.forplay.menu.MenuController;
import me.forplay.model.DataModel;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;

public class MainApp extends Application {
    private org.apache.logging.log4j.Logger logger = LogManager.getLogger(this.getClass().getName());
    public static Stage primaryStage;
    public static Node menuNode;
    public static BorderPane root;

    @Override
    public void start(Stage primaryStage) throws IOException {
        logger.info("MainApp start...");
        MainApp.primaryStage = primaryStage;
         root = new BorderPane();
        FXMLLoader menuLoader = new FXMLLoader(this.getClass().getResource("/fxml/menu/menu.fxml"));
        menuNode = menuLoader.load();
        root.setTop(menuNode);

        MenuController menuController = menuLoader.getController();
        DataModel model = new DataModel();
        menuController.initModel(model);

        FXMLLoader startLoader = new FXMLLoader(this.getClass().getResource("/fxml/start/start.fxml"));
        Node start = startLoader.load();
        root.setCenter(start);

        Scene scene = new Scene(root,1024,768);
        Image icon = new Image(this.getClass().getResourceAsStream("/img/ic_spellcheck_black_48dp.png"));

        primaryStage.setTitle("英语单词测试");
        primaryStage.getIcons().add(icon);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
