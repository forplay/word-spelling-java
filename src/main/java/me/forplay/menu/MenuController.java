package me.forplay.menu;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import me.forplay.application.MainApp;
import me.forplay.model.DataModel;
import me.forplay.test.TestSpellController;
import me.forplay.test.TestTranslateController;

import java.io.IOException;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-07-29
 */
public class MenuController {
    @FXML
    public MenuBar menuBar;
    private DataModel model;

    public void initModel(DataModel model) {
        if (this.model != null) {
            throw new IllegalStateException("Model can only be initialized once");
        }
        this.model = model;
    }

    @FXML
    public void startNew(ActionEvent event) throws IOException {
        TestSpellController.index = 0;
        TestTranslateController.index = 0;
        FXMLLoader startLoader = new FXMLLoader(this.getClass().getResource("/fxml/start/start.fxml"));
        Node startPane = startLoader.load();
        MainApp.root.setCenter(startPane);
    }

    @FXML
    public void testingResult(ActionEvent event) throws IOException {
        FXMLLoader menuLoader = new FXMLLoader(this.getClass().getResource("/fxml/result/result.fxml"));
        Node resultNode = menuLoader.load();
        MainApp.root.setCenter(resultNode);
    }

    @FXML
    public void exit(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    public void editVocabulary(ActionEvent event) throws IOException {
        FXMLLoader menuLoader = new FXMLLoader(this.getClass().getResource("/fxml/menu/editor.fxml"));
        Node menuNode = menuLoader.load();
        MainApp.root.setCenter(menuNode);
    }

    @FXML
    public void manual(ActionEvent event) {

    }

    @FXML
    public void about(ActionEvent event) throws IOException {
        Stage primaryStage = MainApp.primaryStage;
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/menu/about.fxml"));
        Parent load = loader.load();

        Scene sceneAbout = new Scene(load);
        Image icon = new Image(getClass().getResourceAsStream("/img/ic_info_outline_black_48dp.png"));

        stage.getIcons().add(icon);
        stage.setTitle("软件信息");
        stage.setScene(sceneAbout);
        stage.setAlwaysOnTop(true);
        stage.setResizable(false);
        stage.initOwner(primaryStage);

        ChangeListener<Number> widthListener = new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                Double stageWidth = newValue.doubleValue();
                stage.setX(primaryStage.getX() + primaryStage.getWidth() / 2 - stageWidth / 2);
            }
        };
        ChangeListener<Number> heightListener = new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                Double stageHeight = newValue.doubleValue();
                stage.setY(primaryStage.getY() + primaryStage.getHeight() / 2 - stageHeight / 2);
            }
        };
        stage.widthProperty().addListener(widthListener);
        stage.heightProperty().addListener(heightListener);

        //Once the window is visible, remove the listeners
        stage.setOnShown(event1 -> {
            stage.widthProperty().removeListener(widthListener);
            stage.heightProperty().removeListener(heightListener);
        });

        stage.getModality();
        stage.show();

        if (primaryStage != null) {
            primaryStage.getScene().setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    stage.hide();
                }
            });
        }
    }
}
