package me.forplay.menu;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.SplitPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-07-29
 */
public class AboutController implements Initializable {
    @FXML
    public SplitPane splitPane;
    @FXML
    public TextFlow changeLog;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Set SplitPane divider transparent,not working.
        /*
        Node divider = splitPane.lookup(".split-pane-divider");
        if (divider != null) {
            divider.setStyle("-fx-background-color: transparent;");
        }
        */

        String log = "# Changelog\n" +
                "--------------------------------------\n" +
                "0.0.2 - 2018-09-25 - Released\n" +
                "Added\n" +
                "- 显示英文，抢答中文模式\n" +
                "- 保存、读取、删除测试结果 \n" +
                "--------------------------------------\n" +
                "0.0.1 - 2018-08-01 - Released\n" +
                "- Implemented basic functions";
        Text text = new Text(log);
        changeLog.getChildren().add(text);
    }
}
