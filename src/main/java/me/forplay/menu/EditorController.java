package me.forplay.menu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;
import me.forplay.model.Word;
import me.forplay.util.Utils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static me.forplay.util.Utils.showTooltip;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-07-29
 */
public class EditorController implements Initializable {
    @FXML
    public TableView<Word> tableView;
    @FXML
    public TableColumn<Word, Integer> tcId;
    @FXML
    public TableColumn<Word, String> tcChinese;
    @FXML
    public TableColumn<Word, String> tcEnglish;
    @FXML
    public TableColumn<Word, String> tcStatus;
    @FXML
    public TableColumn<Word, Word> tcOperation;
    @FXML
    public TextField chineseField;
    @FXML
    public TextField englishField;
    @FXML
    public Label tipSuccess;

    ObservableList<Word> dataList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tcOperation.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Word, Word>, ObservableValue<Word>>() {
            @Override
            public ObservableValue<Word> call(TableColumn.CellDataFeatures<Word, Word> param) {
                return new ReadOnlyObjectWrapper<>(param.getValue());
            }
        });

        tcOperation.setCellFactory(new Callback<TableColumn<Word, Word>, TableCell<Word, Word>>() {
            @Override
            public TableCell<Word, Word> call(TableColumn<Word, Word> param) {
                return new TableCell<Word, Word>() {
                    private final Button editButton = new Button("修改");
                    private final Button muteButton = new Button("屏蔽");
                    private final Button deleteButton = new Button("删除");
                    private final HBox hBox = new HBox(10.0, editButton, muteButton, deleteButton);

                    @Override
                    protected void updateItem(Word word, boolean empty) {
                        super.updateItem(word, empty);

                        if (word == null) {
                            setGraphic(null);
                            return;
                        }
                        setGraphic(hBox);

                        editButton.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                String english = word.getEnglish();
                                String chinese = word.getChinese();

                                Dialog dialog = new Dialog<Pair<String, String>>();
                                dialog.setTitle(english + "--" + chinese);
                                dialog.setHeaderText(null);

                                // Set the icon (must be included in the project)
                                Image icon = new Image(getClass().getResourceAsStream("/img/ic_edit_black_48dp.png"));
                                Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
                                stage.getIcons().add(icon);

                                ButtonType confirmButtonType = new ButtonType("确定", ButtonBar.ButtonData.OK_DONE);
                                dialog.getDialogPane().getButtonTypes().addAll(confirmButtonType, ButtonType.CANCEL);

                                GridPane grid = new GridPane();
                                grid.setHgap(10.0);
                                grid.setVgap(10.0);
                                grid.setPadding(new Insets(20.0, 150.0, 10.0, 10.0));

                                TextField tfEnglish = new TextField();
                                TextField tfChinese = new TextField();

                                tfEnglish.setText(english);
                                tfChinese.setText(chinese);

                                grid.add(new Label("英文:"), 0, 0);
                                grid.add(tfEnglish, 1, 0);
                                grid.add(new Label("中文："), 0, 1);
                                grid.add(tfChinese, 1, 1);

                                // Enable/Disable  confirm button depending on whether the input was entered.
                                Node confirmButton = dialog.getDialogPane().lookupButton(confirmButtonType);
                                // Do validation
                                tfEnglish.textProperty().addListener(new ChangeListener<String>() {
                                    @Override
                                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                                        confirmButton.setDisable(newValue.trim().isEmpty());
                                    }
                                });

                                tfChinese.textProperty().addListener(new ChangeListener<String>() {
                                    @Override
                                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                                        confirmButton.setDisable(newValue.trim().isEmpty());
                                    }
                                });

                                dialog.getDialogPane().setContent(grid);
                                tfEnglish.requestFocus();

                                // Convert the result to a username-password-pair when the login button is clicked.
                                dialog.setResultConverter(new Callback<ButtonType, Pair<String, String>>() {
                                    @Override
                                    public Pair<String, String> call(ButtonType dialogButton) {
                                        if (dialogButton == confirmButtonType) {
                                            int id = word.getId();
                                            Word word1 = dataList.stream().filter(it -> it.getId() == id).findAny().get();
                                            int index = dataList.indexOf(word1);
                                            word1.setEnglish(tfEnglish.getText());
                                            word1.setChinese(tfChinese.getText());
                                            dataList.set(index, word1);

                                            File file = new File("vocabulary.json");
                                            String json = JSON.toJSONString(dataList);
                                            try {
                                                FileUtils.writeStringToFile(file, json, "utf-8");
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            tableView.getItems().setAll(dataList);

                                            return new Pair<>(tfEnglish.getText(), tfChinese.getText());
                                        } else
                                            return null;
                                    }
                                });

                                Optional<Pair<String, String>> result = dialog.showAndWait();

                                result.ifPresent(new Consumer<Pair<String, String>>() {
                                    @Override
                                    public void accept(Pair<String, String> word) {
                                        System.out.println("english=" + word.getKey() + ", chinese=" + word.getValue());
                                    }
                                });
                            }
                        });

                        muteButton.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                int id = word.getId();
                                Word word1 = dataList.stream().filter(it -> it.getId() == id).findAny().get();
                                int index = dataList.indexOf(word1);
                                if ("已屏蔽".equals(word1.getStatus())) {
                                    word1.setStatus("");
                                } else {
                                    word1.setStatus("已屏蔽");
                                }
                                dataList.set(index, word1);

                                File file = new File("vocabulary.json");
                                String json = JSON.toJSONString(dataList);
                                try {
                                    FileUtils.writeStringToFile(file, json, "utf-8");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                tableView.getItems().setAll(dataList);
                            }
                        });

                        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                String english = word.getEnglish();
                                String chinese = word.getChinese();

                                Dialog dialog = new Dialog<Pair<String, String>>();
                                dialog.setHeaderText("是否删除?");

                                Label content = new Label(english + "--" + chinese);
                                Image icon = new Image(getClass().getResourceAsStream("/img/ic_delete_forever_black_48dp.png"));
                                Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
                                stage.getIcons().add(icon);

                                ButtonType confirmButtonType = new ButtonType("删除", ButtonBar.ButtonData.OK_DONE);
                                dialog.getDialogPane().getButtonTypes().addAll(confirmButtonType, ButtonType.CANCEL);

                                TextField tfEnglish = new TextField();
                                TextField tfChinese = new TextField();
                                tfEnglish.setText(english);
                                tfChinese.setText(chinese);

                                dialog.getDialogPane().setContent(content);
                                tfEnglish.requestFocus();

                                dialog.setResultConverter(new Callback<ButtonType, Pair<String, String>>() {
                                    @Override
                                    public Pair<String, String> call(ButtonType dialogButton) {
                                        if (dialogButton == confirmButtonType) {
                                            dataList.remove(word);

                                            File file = new File("vocabulary.json");
                                            String json = JSON.toJSONString(dataList);
                                            try {
                                                FileUtils.writeStringToFile(file, json, "utf-8");
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            tableView.getItems().setAll(dataList);

                                            return new Pair<>(tfEnglish.getText(), tfChinese.getText());
                                        } else
                                            return null;
                                    }
                                });
                                Optional<Pair<String, String>> result = dialog.showAndWait();

                                result.ifPresent(new Consumer<Pair<String, String>>() {
                                    @Override
                                    public void accept(Pair<String, String> word) {
                                        System.out.println("english=" + word.getKey() + ", chinese=" + word.getValue());
                                    }
                                });

                            }
                        });
                    }
                };
            }
        });
        tcId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcChinese.setCellValueFactory(new PropertyValueFactory<>("chinese"));
        tcEnglish.setCellValueFactory(new PropertyValueFactory<>("english"));
        tcStatus.setCellValueFactory(new PropertyValueFactory<>("status"));

        tcStatus.setCellFactory(new Callback<TableColumn<Word, String>, TableCell<Word, String>>() {
            @Override
            public TableCell<Word, String> call(TableColumn<Word, String> column) {
                return new TableCell<Word, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        setText(empty ? "" : getItem());
                        setGraphic(null);
                        TableRow<Word> currentRow = getTableRow();
                        if (!isEmpty()) {
                            tcStatus.setStyle("-fx-alignment: CENTER;");
                            if ("已屏蔽".equals(item)) {
                                currentRow.setStyle("-fx-background-color:gray;");
                            } else {
                                currentRow.setStyle(null);
                            }
                        }
                    }
                };
            }
        });

        Path filePath = Utils.getFilePath("vocabulary.json");
        File file = new File(filePath.toUri());
        try {
            String readFileToString = FileUtils.readFileToString(file, "utf-8");
            ArrayList wordList = JSON.parseObject(readFileToString, new TypeReference<ArrayList<Word>>() {
            });
            dataList = FXCollections.observableArrayList(wordList);
            tableView.getItems().setAll(dataList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void addWord(ActionEvent event) {
        String english = englishField.getText();
        String chinese = chineseField.getText();

        englishField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.trim().isEmpty()) {
                    englishField.setPromptText("请输入英文");
                }
            }
        });

        chineseField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.trim().isEmpty()) {
                    chineseField.setPromptText("请输入中文");
                }
            }
        });

        if (englishField.getText().trim().isEmpty() || chineseField.getText().trim().isEmpty()) {
            return;
        }

        List<Integer> list = dataList.stream().map(it -> it.getId()).sorted().collect(Collectors.toList());

        int index = 0;
        boolean flag = false;
        boolean contains = list.contains(1);
        System.out.println("contains:" + contains);
        if (!contains) {
            index = 1;
        } else {
            for (int i = 0; i < list.size() - 1; i++) {
                if (list.get(i + 1) - list.get(i) > 1) {
                    index = list.get(i) + 1;
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                index = dataList.size() + 1;
            }
        }
        Word word = new Word(index, "", english, chinese);
        ObservableList<Word> data = tableView.getItems();
        data.add(word);
        dataList.add(word);

        File file = new File("vocabulary.json");
        String json = JSON.toJSONString(dataList);
        try {
            FileUtils.writeStringToFile(file, json, "utf-8");
            tableView.getItems().setAll(dataList);

            showTooltip(tipSuccess, "添加成功", Color.GREEN, 3);

            englishField.clear();
            chineseField.clear();
            chineseField.requestFocus();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
