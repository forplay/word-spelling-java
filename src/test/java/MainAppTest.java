import com.sun.javafx.scene.control.skin.ContextMenuContent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import me.forplay.application.MainApp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.matcher.control.MenuItemMatchers;

import java.io.IOException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.loadui.testfx.GuiTest.find;
import static org.testfx.api.FxAssert.verifyThat;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-12-26
 */

public class MainAppTest extends ApplicationTest {
    public static Stage primaryStage;
    public static Node menuNode;
    public static BorderPane root;

    @Override
    public void start(Stage primaryStage) throws IOException {
        MainApp.primaryStage = primaryStage;
        root = new BorderPane();
        FXMLLoader menuLoader = new FXMLLoader(this.getClass().getResource("/fxml/menu/menu.fxml"));
        menuNode = menuLoader.load();
        root.setTop(menuNode);

        Scene scene = new Scene(root, 1024, 768);

        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.toFront();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @Test
    public void testMenu() {
        MenuBar menuBar = find("#menuBar");
        assertThat(menuBar.getMenus().size(), is(3));

        assertThat(menuBar.getMenus().get(0).getText(), is("文件"));
        assertThat(menuBar.getMenus().get(1).getText(), is("设置"));
        assertThat(menuBar.getMenus().get(2).getText(), is("帮助"));

        clickOn("#file");
        verifyThat(((ContextMenuContent.MenuItemContainer) find("#newTest")).getItem(), MenuItemMatchers.hasText("新的测试"));
        verifyThat(((ContextMenuContent.MenuItemContainer) find("#testResult")).getItem(), MenuItemMatchers.hasText("测试结果"));
        verifyThat(((ContextMenuContent.MenuItemContainer) find("#exit")).getItem(), MenuItemMatchers.hasText("退出"));

        moveTo("#setting");
        verifyThat(((ContextMenuContent.MenuItemContainer) find("#editLibrary")).getItem(), MenuItemMatchers.hasText("编辑词库"));

        moveTo("#help");
        verifyThat(((ContextMenuContent.MenuItemContainer) find("#about")).getItem(), MenuItemMatchers.hasText("关于"));
        verifyThat(((ContextMenuContent.MenuItemContainer) find("#manual")).getItem(), MenuItemMatchers.hasText("手册"));
    }
}