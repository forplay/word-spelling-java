### introduction
1. reference
   1. https://stackoverflow.com/questions/32342864/applying-mvc-with-javafx
   2. https://stackoverflow.com/questions/48277212/javafx-mvc-application-best-practices-with-database
   3. http://functiontomymadness.blogspot.com/2017/07/hello-world-in-javafx-with-kotlin-and.html
   4. https://github.com/FibreFoX/javafx-gradle-plugin
   5. https://medium.com/information-and-technology/test-driven-development-in-javafx-with-testfx-66a84cd561e0
   
2. deploy executable application
    1. run build.gradle task "javafx/jfxNative"
    2. run  "build/jfx/native/word-spelling/word-spelling.exe"
3. todo list
    1. 新增显示英文，抢答中文模式
    2. 保存和读取测试结果 